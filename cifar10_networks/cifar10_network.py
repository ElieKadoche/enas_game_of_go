import argparse
import os

import numpy as np
import tensorflow as tf
import tensorflow.keras.layers as layers
import tensorflow.keras.regularizers as regularizers
from tensorflow.keras.callbacks import CSVLogger, ModelCheckpoint
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.models import Model, load_model

from networks import *

tf.keras.backend.clear_session()

# Parser configuration
# -------------------------

parser = argparse.ArgumentParser(
    description="Script to train / test a neural network for CIFAR 10"
)
parser.add_argument(
    "mode",
    help="You can either train the network, get info (save a summaray of the model and evaluate the network), or both. Default is all",
    choices=["train", "info", "all"],
    type=str,
    default="all",
)
parser.add_argument(
    "-t",
    "--model-type",
    help="The type of network to use. Default is resnet",
    choices=["resnet", "enas", "simple"],
    default="resnet",
)
parser.add_argument(
    "-w",
    "--weights",
    help="h5 file from which load (if it exists) and save the model weights. Default is cifar10_model.h5",
    type=str,
    default="cifar10_model.h5",
)
parser.add_argument(
    "--output-log",
    help="Output file name of training info. Default is cifar10_log.csv",
    type=str,
    default="cifar10_log.csv",
)
parser.add_argument(
    "--output-info",
    help="Output file name of model info. Default is cifar10_info.txt",
    type=str,
    default="cifar10_info.txt",
)
parser.add_argument(
    "--gpu",
    help="The ID of the GPU (ordered by PCI_BUS_ID) to use. If not set, no GPU configuration is done. Default is None",
    type=int,
    default=None,
)
parser.add_argument(
    "-e", "--epochs", help="Number of epochs. Default is 20", type=int, default=20
)
parser.add_argument(
    "-b", "--batch-size", help="Batch size. Default is 32", type=int, default=32
)
parser.add_argument(
    "-v",
    "--verbose",
    help="If set, output details of the execution",
    action="store_true",
)
parser.add_argument(
    "--tf-log-level",
    help="Tensorflow minimum cpp log level. Default is 0",
    choices=["0", "1", "2", "3"],
    default="0",
)

# Global parameters
# -------------------------

args = parser.parse_args()
verbose = args.verbose

os.environ["TF_CPP_MIN_LOG_LEVEL"] = args.tf_log_level

# GPU configuration
# -------------------------

if args.gpu is not None:
    if verbose:
        print("GPU configuration...", end="")

    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu)

    if verbose:
        print(" Done!")

# Get data
# -------------------------

if verbose:
    print("Getting data...", end="")

nb_classes = 10
input_shape = (32, 32, 3)

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

x_train = x_train.astype("float32") / 255
x_test = x_test.astype("float32") / 255

y_train = tf.keras.utils.to_categorical(y_train, num_classes=nb_classes)
y_test = tf.keras.utils.to_categorical(y_test, num_classes=nb_classes)

if verbose:
    print(" Done!")

# Build model
# -------------------------

if verbose:
    print("Building model...", end="")

if args.model_type == "resnet":
    model = build_resnet_network(input_shape, nb_classes)

elif args.model_type == "enas":
    model = build_enas_network(input_shape, nb_classes)

elif args.model_type == "simple":
    model = build_simple_network(input_shape, nb_classes)

if verbose:
    print(" Done!")

# Model compilation
# -------------------------

if verbose:
    print("Compiling model...", end="")

model.compile(
    loss=tf.keras.losses.categorical_crossentropy,
    optimizer=tf.keras.optimizers.Adam(),
    metrics=["accuracy"],
)

if verbose:
    print(" Done!")

# Loading weights
# -------------------------

if args.weights is not None and os.path.exists(args.weights):
    if verbose:
        print("Loading {}...".format(args.weights), end="")

    model.load_weights(args.weights)

    if verbose:
        print(" Done!")

# Train mode
# -------------------------

if args.mode == "train" or args.mode == "all":
    if verbose:
        print("Training model...")

    csv_logger = CSVLogger(args.output_log, append=True, separator=";")
    checkpoint = ModelCheckpoint(args.weights, verbose=int(verbose), save_freq="epoch")

    history = model.fit(
        x_train,
        y_train,
        batch_size=args.batch_size,
        validation_data=(x_test, y_test),
        epochs=args.epochs,
        verbose=int(verbose),
        callbacks=[csv_logger, checkpoint],
    )

    if verbose:
        print("Saving weights...", end="")

    model.save(args.weights)

    if verbose:
        print(" Done!")

# Info mode
# -------------------------

if args.mode == "info" or args.mode == "all":
    if verbose:
        print("Evaluating the model...", end="")

    metrics = model.evaluate(x_test, y_test, batch_size=args.batch_size, verbose=0)

    summary_list = []
    model.summary(print_fn=lambda x: summary_list.append(x))
    model_summary = "\n".join(summary_list)

    if verbose:
        print(" Done!")
        print("Saving info...")

    info = "Model evaluation\n# -------------------------\n\n"

    for index_metric in range(len(model.metrics_names)):
        info += "{}: {}\n".format(
            model.metrics_names[index_metric], metrics[index_metric]
        )

    info += "\nModel summary\n# -------------------------\n\n"
    info += model_summary

    if verbose:
        print(info)

    with open(args.output_info, "w") as f:
        f.write(info)
