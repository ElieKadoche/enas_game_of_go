import tensorflow.keras.layers as layers
import tensorflow.keras.regularizers as regularizers
from tensorflow.keras.models import Model

# Simple network
# --------------------


def build_simple_network(input_shape, nb_classes):
    dropout = 0.2

    X = layers.Input(shape=input_shape)

    network = layers.Conv2D(
        16,
        activation=None,
        kernel_size=(3, 3),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(1e-3),
    )(X)
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)

    network = layers.Conv2D(
        16,
        activation=None,
        kernel_size=(3, 3),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(1e-3),
    )(network)
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)

    network = layers.Conv2D(
        16,
        activation=None,
        kernel_size=(3, 3),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(1e-3),
    )(network)
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)

    network = layers.AveragePooling2D()(network)
    network = layers.Flatten()(network)
    network = layers.Dense(nb_classes, activation="softmax")(network)
    return Model(inputs=X, outputs=network)


# ResNet network
# --------------------


def build_resnet_network(input_shape, nb_classes):
    depth = 20
    num_filters = 32

    X = layers.Input(shape=input_shape)
    network = layers.Conv2D(
        filters=num_filters,
        kernel_size=(3, 3),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(1e-3),
    )(X)
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)

    for stack in range(3):
        for residual_block in range(int((depth - 2) / 6)):

            strides = 1
            if stack > 0 and residual_block == 0:
                strides = 2

            y = layers.Conv2D(
                filters=num_filters,
                kernel_size=(3, 3),
                strides=strides,
                padding="same",
                kernel_initializer="he_normal",
                kernel_regularizer=regularizers.l2(1e-3),
            )(network)
            y = layers.BatchNormalization()(y)
            y = layers.Activation("relu")(y)

            y = layers.Conv2D(
                filters=num_filters,
                kernel_size=(3, 3),
                padding="same",
                kernel_initializer="he_normal",
                kernel_regularizer=regularizers.l2(1e-3),
            )(y)
            y = layers.BatchNormalization()(y)

            if stack > 0 and residual_block == 0:
                network = layers.Conv2D(
                    filters=num_filters,
                    kernel_size=(1, 1),
                    padding="same",
                    strides=strides,
                    kernel_initializer="he_normal",
                    kernel_regularizer=regularizers.l2(1e-3),
                )(network)

            network = layers.Add()([network, y])
            network = layers.Activation("relu")(network)

        num_filters *= 2

    # Add classifier on top
    network = layers.AveragePooling2D(pool_size=8)(network)
    network = layers.Flatten()(network)
    network = layers.Dense(
        nb_classes, activation="softmax", kernel_initializer="he_normal"
    )(network)

    return Model(inputs=X, outputs=network)


# ENAS networks
# --------------------


def build_enas_network(input_shape, nb_classes):
    nb_filters = 96
    reg_value = 2e-4
    dropout = 0.4

    X = layers.Input(shape=input_shape)

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(X)
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_0 = network

    network = layers.SeparableConv2D(
        filters=nb_filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        depthwise_initializer="he_normal",
        depthwise_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_1 = network

    network = layers.MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_2 = network

    network = layers.SeparableConv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        depthwise_initializer="he_normal",
        depthwise_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_3 = network

    network = layers.AveragePooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.Add()([network, layer_1, layer_2])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_4 = network

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()([network, layer_0, layer_2, layer_3])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_5 = network

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()([network, layer_1])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_6 = network

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_7 = network

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()([network, layer_1, layer_4, layer_5, layer_7])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_8 = network

    network = layers.MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.Add()([network, layer_0, layer_5, layer_6, layer_7, layer_8])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_9 = network

    network = layers.MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.Add()(
        [
            network,
            layer_1,
            layer_2,
            layer_3,
            layer_4,
            layer_5,
            layer_6,
            layer_8,
            layer_9,
        ]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_10 = network

    network = layers.SeparableConv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        depthwise_initializer="he_normal",
        depthwise_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()(
        [network, layer_1, layer_2, layer_3, layer_5, layer_6, layer_8, layer_10]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_11 = network

    network = layers.SeparableConv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        depthwise_initializer="he_normal",
        depthwise_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()([network, layer_0, layer_1, layer_6, layer_8, layer_10])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_12 = network

    network = layers.MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.Add()([network, layer_1, layer_4, layer_5, layer_10])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_13 = network

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()(
        [network, layer_1, layer_2, layer_4, layer_5, layer_6, layer_12]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_14 = network

    network = layers.MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.Add()(
        [network, layer_0, layer_1, layer_3, layer_11, layer_12, layer_14]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_15 = network

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()([network, layer_2, layer_7, layer_9, layer_13])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_16 = network

    network = layers.MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.Add()(
        [network, layer_3, layer_4, layer_5, layer_7, layer_10, layer_11]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_17 = network

    network = layers.MaxPooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.Add()([network, layer_1, layer_5, layer_6, layer_14, layer_16])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_18 = network

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()(
        [
            network,
            layer_2,
            layer_6,
            layer_7,
            layer_9,
            layer_10,
            layer_12,
            layer_14,
            layer_16,
            layer_17,
        ]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_19 = network

    network = layers.SeparableConv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        depthwise_initializer="he_normal",
        depthwise_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()(
        [
            network,
            layer_0,
            layer_3,
            layer_8,
            layer_10,
            layer_12,
            layer_14,
            layer_16,
            layer_17,
            layer_19,
        ]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_20 = network

    network = layers.AveragePooling2D(pool_size=(3, 3), strides=(1, 1), padding="same")(
        network
    )
    network = layers.Add()([network, layer_9, layer_10, layer_13, layer_14, layer_20])
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_21 = network

    network = layers.Conv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        kernel_initializer="he_normal",
        kernel_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()(
        [
            network,
            layer_3,
            layer_6,
            layer_8,
            layer_12,
            layer_15,
            layer_17,
            layer_18,
            layer_19,
        ]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)
    layer_22 = network

    network = layers.SeparableConv2D(
        filters=nb_filters,
        kernel_size=(5, 5),
        strides=(1, 1),
        padding="same",
        depthwise_initializer="he_normal",
        depthwise_regularizer=regularizers.l2(reg_value),
    )(network)
    network = layers.Add()(
        [
            network,
            layer_3,
            layer_4,
            layer_6,
            layer_7,
            layer_10,
            layer_16,
            layer_17,
            layer_21,
        ]
    )
    network = layers.BatchNormalization()(network)
    network = layers.Activation("relu")(network)
    network = layers.Dropout(dropout)(network)

    network = layers.Flatten()(network)
    network = layers.Dense(nb_classes, activation="softmax")(network)
    return Model(inputs=X, outputs=network)
