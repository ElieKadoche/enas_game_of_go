import re
import argparse
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser()
parser.add_argument("file_name", help="The name of the ENAS output file to parse")
file_name = parser.parse_args().file_name

# -------------
# Valid dataset
# -------------

content = [re.findall(r'val_acc=(1.0+|0.\d+)', line) for line in open(file_name)]
content = [float(c[0]) for c in content if c]

avg_values = list()
max_values = list()

for epoch in zip(*(iter(content),) * 10):
    avg_values.append(sum(epoch) / len(epoch))
    max_values.append(max(epoch))

max_value_avg = max(avg_values)
indexes_max_values_avg = [i for i in range(len(avg_values)) if avg_values[i] == max_value_avg]

max_value_max = max(max_values)
indexes_max_values_max = [i for i in range(len(max_values)) if max_values[i] == max_value_max]

# ------------
# Test dataset
# ------------

content_test_accuracy = [re.findall(r'test_eaccuracy: (1.0+|0.\d+)', line) for line in open(file_name)]
content_test_accuracy = [float(c[0]) for c in content_test_accuracy if c]

max_test_values = max(content_test_accuracy)
indexes_max_test_values = [i for i in range(len(content_test_accuracy)) if content_test_accuracy[i] == max_test_values]

# ----------
# Graph plot
# ----------

fig, axs = plt.subplots(3, sharex=True, sharey=True, figsize=(12.40, 8.19))
fig.suptitle('Epochs. Graphs 1 and 2 are on valid dataset and graph 3 is on test dataset.\nFile: {}'.format(file_name))

axs[0].plot(avg_values, '-', color='#202F3C', linewidth=1)
for max_point in indexes_max_values_avg:
    axs[0].plot(max_point, max_value_avg, 'x', color="#5B2C6F")
    axs[0].text(max_point, max_value_avg, '({}, {})'.format(max_point, round(max_value_avg, 4)), fontsize=7)
axs[0].set_ylabel('Average accuracy values')
axs[0].axis([0, 310, 0, 1])

axs[1].plot(max_values, "-", color="#9A7D0A", linewidth=1)
for max_point in indexes_max_values_max:
    axs[1].plot(max_point, max_value_max, 'x', color="#5B2C6F")
    axs[1].text(max_point, max_value_max, '({}, {})'.format(max_point, round(max_value_max, 4)), fontsize=7)
axs[1].set_ylabel("Max accuracy values")
axs[1].axis([0, 310, 0, 1])

axs[2].plot(content_test_accuracy, "-", color="#5B2C6F", linewidth=1)
for max_point in indexes_max_test_values:
    axs[2].plot(max_point, max_test_values, 'x', color="#5B2C6F")
    axs[2].text(max_point, max_test_values, '({}, {})'.format(max_point, round(max_test_values, 4)), fontsize=7)
axs[2].set_ylabel("Test accuracy")
axs[2].axis([0, 310, 0, 1])

# plt.show()
plt.savefig('{}_graph.png'.format(file_name), dpi=400)
