To dowload data, you can either follow the instructions given [here](https://gitlab.com/ElieKadoche/baduk_nets.git) or execute the following commands.

```
wget https://www.lamsade.dauphine.fr/~cazenave/DeepLearningProject.zip
unzip DeepLearningProject.zip
mv DeepLearningProject/*.npy ./
rm -r DeepLearningProject
rm DeepLearningProject.zip
```
