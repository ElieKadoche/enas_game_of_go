# enas_game_of_go

The adaptation of ENAS for the game of Go.

Initial repository: https://github.com/melodyguan/enas.git.

Paper: https://arxiv.org/abs/1802.03268

Authors: Hieu Pham*, Melody Y. Guan*, Barret Zoph, Quoc V. Le, Jeff Dean.

## Structure

In the `src` folder, there is the source code for ENAS. There is one source code for the CIFAR10 version, and one for the game of Go version.

In the `scripts` folder, there are all the scripts you can use to execute ENAS.

`outputs_saved` contains saved outputs from executed ENAS scripts.

`cifar10_networks` contains CIFAR10 networks and `data` contains the datasets used by ENAS.

## Important

To create data for baduk, go into the `data/baduk` folder and follow instructions.

In `general_child.py`, you need to choose the right parameters in the following functions: `_model` and `_build_train`, depending on what you want to use ENAS for (policy or value).

## Special comments

Modification from the original repository are noted with 'Correction from original'.

## CIFAR-10

A macro architecture for a neural network with `N` layers consists of `N` parts, indexed by `1, 2, 3, ..., N`. Part `i` consists of:

- a number in `[0, 1, 2, 3, 4, 5]` that specifies the operation at layer `i`-th, corresponding to `conv_3x3`, `separable_conv_3x3`, `conv_5x5`, `separable_conv_5x5`, `average_pooling`, `max_pooling`.
- A sequence of `i - 1` numbers, each is either `0` or `1`, indicating whether a skip connection should be formed from a the corresponding past layer to the current layer.

## Baduk

A macro architecture for a neural network with `N` layers consists of `N` parts, indexed by `1, 2, 3, ..., N`. Part `i` consists of:

- a number in `[0, 1, 2, 3, 4, 5]` that specifies the operation at layer `i`-th, corresponding to `conv_3x3`, `separable_conv_3x3`, `conv_1x1`, `separable_conv_1x1`, `average_pooling`, `max_pooling`.
- A sequence of `i - 1` numbers, each is either `0` or `1`, indicating whether a skip connection should be formed from a the corresponding past layer to the current layer.
