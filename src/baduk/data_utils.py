import os
import sys

import numpy as np
from sklearn.model_selection import train_test_split


def read_data(data_path, num_valids=8333):
    print("-" * 80)
    print("Reading data")

    images, labels = {}, {}

    features = np.load("data/baduk/input_data.npy")  # (100000, 19, 19, 8)
    policies = np.load("data/baduk/policy.npy")  # (100000, 361)
    values = np.load("data/baduk/value.npy")  # (100000,)

    features = np.float32(features)
    policies = np.float32(policies)
    values = np.float32(values)

    x_train, x_test, y_train, y_test = train_test_split(
        features,
        values,
        # policies,
        test_size=0.2,
        shuffle=True,
    )

    images["train"], labels["train"] = x_train, y_train

    if num_valids:
        images["valid"] = images["train"][-num_valids:]
        labels["valid"] = labels["train"][-num_valids:]

        images["train"] = images["train"][:-num_valids]
        labels["train"] = labels["train"][:-num_valids]
    else:
        images["valid"], labels["valid"] = None, None

    images["test"], labels["test"] = x_test, y_test

    return images, labels
